import requests
import time
import logging


class HCaptchaGuru:
    def __init__(self, captcha_guru_key,  logger: logging.Logger,
                 captcha_try=40,
                 sleep_between=10):
        '''
        methods: userrecaptcha,
        '''
        self._captcha_guru_key = captcha_guru_key
        self._captcha_try = captcha_try
        self._sleep_between = sleep_between
        self._logger = logger

    @property
    def captcha_guru_key(self):
        return self._captcha_guru_key

    @captcha_guru_key.setter
    def captcha_guru_key(self, captcha_guru_key):
        self._captcha_guru_key = captcha_guru_key

    def recognizer(self, site_key, page_url):
        for _ in range(5):
            try:
                return self.recognize(site_key, page_url)
            except Exception as err:
                self._logger.error(err)
                self._logger.info("Try again!")
            time.sleep(5)

    def recognize(self, site_key, page_url):
        '''Распознаем'''
        captcha_id = self._send_captcha(site_key, page_url)
        if captcha_id:
            for _ in range(self._captcha_try):
                time.sleep(self._sleep_between)
                res = self._get_captcha(captcha_id)
                if res.startswith('OK|'):
                    # self._logger.info(f"Captcha result: {res}")
                    return res.split('|')[1]
                self._logger.info(res)
                if 'ERROR_CAPTCHA_UNSOLVABLE' in res:
                    break

        raise Exception(message=f"Can't recognize captcha with captcha_id: {captcha_id}| {res}")
        # return False

    def _get_captcha(self, id):
        return requests.get(
            f'http://api.captcha.guru/res.php?key={self._captcha_guru_key}&action=get&id={id}').content.decode()

    def _send_captcha(self, site_key, page):
        content = requests.get(
            f'http://api.captcha.guru/in.php?key={self._captcha_guru_key}&method=hcaptcha&sitekey={site_key}&pageurl={page}').content.decode()
        self._logger.debug(f'http://api.captcha.guru/in.php?key={self._captcha_guru_key}&method=hcaptcha&sitekey={site_key}&pageurl={page}')
        p = content.split('|')
        if p[0] == 'OK':
            self._logger.info(f"Sended captcha: {p[1]}")
            return p[1]
        # print(content)
        raise Exception(message=f"Can't send captcha")
        # return False
