import requests
import lxml.html
from hcaptcha_guru import HCaptchaGuru
import config as cfg
import logging
import re
import json
from multiprocessing.pool import ThreadPool
from bmw_saver import BmwSaver

class BmwCats:
    def __init__(self, captcha: HCaptchaGuru, bmwsaver:BmwSaver, logger: logging.Logger):
        self._logger = logger
        self._captcha = captcha
        self._bmwsaver = bmwsaver
        self.session = requests.Session()
        self.session.headers[
            'User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) ' \
                            'AppleWebKit/537.36 (KHTML, like Gecko) ' \
                            'Chrome/85.0.4183.83 Safari/537.36'

    def check_h_captcha(fn):
        def wrapper(self, *args, **kwargs):
            content = fn(self, *args, **kwargs)
            if content and ('h-captcha' in content):
                # with open('1.txt', 'w') as f:
                #     f.write(content)
                html = lxml.html.fromstring(content)
                token = html.xpath('.//input[@id="pageToken"]')[0].value
                sign = html.xpath('.//input[@id="pageSign"]')[0].value
                # print(token, sign)
                captcha_resp = self._captcha.recognizer(cfg.hcaptcha_site_key,
                                                       'https://www.bmwcats.com/')
                # print(captcha_resp)
                self.session.post('https://www.bmwcats.com/_hcaptcha', data={
                    'response': captcha_resp,
                    'token': token,
                    'sign': sign
                })
                content = fn(self, *args, **kwargs)
                return content
            return content

        return wrapper

    @check_h_captcha
    def _go_to_main(self):
        resp = self.session.get(
            f'https://www.bmwcats.com')
        content = resp.content.decode()
        if len(content) > 100:
            return content
        return self._go_to_main()

    @check_h_captcha
    def go_to_vin(self, vin):
        resp = self.session.get(
            f'https://www.bmwcats.com/ajax_vin_bmw.php?vin={vin}').content.decode()
        if 'Такой ВИН не найден в базе данных' in resp:
            return False
        # print(resp)
        return resp

    @check_h_captcha
    def _check_vin(self, vin):
        resp = self.session.get(
            f'https://www.bmwcats.com/bmw/G30/58524/L:N:201701/?vin={vin}')
        content = resp.content.decode()
        return content

    def _get_model_info(self, content):
        '''Получить информацию из верхнего блока (если она есть) - неполная информация'''

        html = lxml.html.fromstring(content)
        names = html.xpath('.//span[@class="etk-mospid-carinfo-topic"]')
        values = html.xpath('.//span[@class="etk-mospid-carinfo-value"]')
        if names and values:
            res = {}
            for name, value in zip(names, values):
                span = value.xpath('./span')
                if span:
                    value = span[0]
                res[name.text.replace(':', '').strip()] = value.text
            return res
        return False

    def _get_full_info(self, content):
        '''Вернуть информацию со страницы комплектации и сведений об авто - полная информация'''
        html = lxml.html.fromstring(content)
        res = {}
        tables = html.xpath('.//div[@class="bmw-asap-carinfo-wrapper"]')
        if tables:
            for table in tables:
                header = table.xpath(
                    './/*[local-name() = "h2" or local-name() = "h3"]')[
                    0].text.strip()
                res[header] = {}
                names = table.xpath(
                    './/div[@class="div-td-name" or @class="div-td-opt-code"]/span')
                values = table.xpath(
                    './/div[@class="div-td-value" or @class="div-td-opt-value"]/span')
                for name, value in zip(names, values):
                    res[header][name.text] = value.text
            return res
        return False

    def _get_vin_options(self, content):
        '''парсит результат запроса по вину, возвращает результаты в виде словаря'''
        res = {}
        res['top'] = self._get_model_info(content)
        g = re.search(r'queryOptionsByVin\(\{([^\}]+)\}\)', content)
        if g:
            # return "OK"
            opt = g.group(1).replace('&quot;', '').split(',')
            # print(opt)
            query = {}
            for item in [item for item in opt]:
                x = item.split(":")
                query[x[0]] = x[1]
            optionsJson = self.session.post(
                'https://www.bmwcats.com/ajax_vin_query.php', query).json()
            opts = self._get_full_info(optionsJson['data'])
            if opts:
                res = {**res, **opts}
        # print('NO')
        return res

    def get_info_by_vin(self, vin):
        self._logger.info(f"Parse info for {vin}")

        res = {}
        res['vin'] = vin
        res['top'] = {}
        res['Сведения об автомобиле'] = {}
        res['Комплектация'] = {}
        if not self._bmwsaver.it_new_vin(vin):
            logger.info(f"{vin} already parsed.")
        else:
            if self.go_to_vin(vin):
                html = self._check_vin(vin)
                res_op = self._get_vin_options(html)
                res = {**res, **res_op}
        return res

    def run(self, vins):
        self._go_to_main()  # для разгадывания капчи
        # for vin in vins:
        #     self.go_to_vin(vin)
        #
        # exit()

        # content = bwm.get_info_by_vin(vins[0])
        pool = ThreadPool(cfg.threads)
        imap_unordered_it = pool.imap_unordered(self.get_info_by_vin, vins)
        for i, results in enumerate(imap_unordered_it):
            # dkdb.save_car_parse_resuls(results['input_id'], results['results'])
            print(f'Parsed {i + 1} from {len(vins)} ')
            # print(results)
            bmw_result = {**results['top'],
                          **results['Сведения об автомобиле'],
                          'comp': results['Комплектация'],
                          'vin': results['vin']}
            logger.info(f"{bmw_result}")
            self._bmwsaver.add_bwm_result(bmw_result)
            # print(f"{bmw_result}")
            # logger.info(f"{bmw_result}")
            # print('_________')
        pool.close()
        pool.join()


if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    f_handler = logging.FileHandler(cfg.log_file, 'a+', 'utf-8')
    f_handler.setFormatter(logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
    f_handler.setLevel(logging.INFO)
    logger.addHandler(f_handler)
    logger.setLevel(logging.INFO)
    captcha = HCaptchaGuru(cfg.captcha_guru_key, logger)
    bmwsaver = BmwSaver(cfg.result_file)
    bwm = BmwCats(captcha, bmwsaver, logger)
    with open(cfg.vin_file) as f:
        vins = [l.strip() for l in f.readlines()]
    bwm.run(vins)

