Установка парсера (Windows).
    1. Для установки парсера в Windows среду, требуется установленный python3.7,
    git

    https://www.python.org/ftp/python/3.7.9/python-3.7.9-amd64.exe
    https://git-scm.com/download/win

    2. Откройте cmd и перейдите в C: (можно в любое другое место)
        cd \
    3. Скайте парсер:

        git clone https://supspec@bitbucket.org/supspec/mih_bmw.git

        cd mih_bmw

    4. Создание виртуального окружения и его активация
        python -m venv venv
        venv\Scripts\activate

    5. Установка зависимостей:
        pip install -r requirements.txt

    6. Скопируйте файл:
        cp config-sample.py config.py

    !!!!! В файле config.py заполните параметр captcha_guru_key

Другие параметры config.py:
    hcaptcha_site_key - это ключ к сервису hcaptcha (не менять)
    captcha_guru_key = '' # ключ от captcha.guru
    vin_file = 'vins.txt' # файл с винами для парсинга

    threads = 10    # количество потоков (10 вполне достаточно)
    result_file = 'results.xls' # результаты парсинга
    log_file = 'log.txt'  # путь до файла логов, стоит переодически удалять