import xlwt
from xlwt import Workbook
import xlrd
from xlutils.copy import copy
import os


class BmwSaver:
    headers = ['vin', 'Модель', 'Регион', 'Кузов', 'Дата изготовления',
               'Код', 'Тип', 'Серия', 'Линия', 'Руль', 'Дверей',
               'Мотор', 'Объем', 'Привод', 'КПП', 'Цвет', 'Обивка',
               'Изготовлено'
               ]

    def __init__(self, filename):
        self._file = filename
        self._vins = []
        self.wb_init()

    def it_new_vin(self, vin):
        if vin in self._vins:
            return False
        return True

    def add_bwm_result(self, result):
        if self.it_new_vin(result['vin']):
            row = self._nrows
            for i, name in enumerate(self.headers):
                self._worksheet.write(row, i, result.get(name, '-'))
            x = len(self.headers)
            for i, key in enumerate(result['comp'].keys()):
                # v = str(result['comp'].get(key, '-'))
                self._worksheet.write(row, x + i,
                                      f"{key}:{result['comp'].get(key, '-')}")
            self._vins.append(result['vin'])
            self._workbook.save(self._file)
            self._nrows = self._nrows + 1

    def wb_init(self):
        if os.path.exists(self._file):
            rb = xlrd.open_workbook(self._file, formatting_info=True)
            sheet = rb.sheet_by_index(0)
            self._nrows = sheet.nrows
            for i in range(1, sheet.nrows):
                self._vins.append(sheet.cell_value(i, 0))
            self._workbook = copy(rb)
            self._worksheet = self._workbook.get_sheet(0)

        else:
            self._workbook = xlwt.Workbook()
            self._worksheet = self._workbook.add_sheet('Sheet 1')
            for i, head in enumerate(self.headers):
                self._worksheet.write(0, i, head)
            self._workbook.save(self._file)
            self._nrows = 1


if __name__ == '__main__':
    saver = BmwSaver('result.xls')
